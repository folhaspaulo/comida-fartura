(function(){

var arteFolha = arteFolha || {},
	url = (window.location.href.indexOf('staging') >= 0 ? 'http://staging.media.folha.com.br/' : 'http://media.folha.uol.com.br/') + 'comida/2016/06/13/quiz-especial-fartura.json',
	// url = './json/quiz.json',
	touches = [],
	quizData = [],
	quizGam = [],
	score = 0,
	corretas = [];

	arteFolha.utils = {
		isEven: function(n){
			return (n % 2 == 0);
		},
		cleanUp: function(value){
			return value.replace(/--/g, '\u2013')
						.replace(/\b'\b/g, "\u2019")
						.replace(/'\b/g, "\u2018")
						.replace(/'/g, "\u2019")
						.replace(/"\b/g, "\u201c")
						.replace(/"â/g,"\u201câ")
						.replace(/"ê/g,"\u201cê")
						.replace(/"ô/g,"\u201cô")
						.replace(/"ã/g,"\u201cã")
						.replace(/"õ/g,"\u201cõ")
						.replace(/"Á/g,"\u201cÁ")
						.replace(/"É/g,"\u201cÉ")
						.replace(/"Í/g,"\u201cÍ")
						.replace(/"Ó/g,"\u201cÓ")
						.replace(/"Ú/g,"\u201cÚ")
						.replace(/"á/g,"\u201cá")
						.replace(/"é/g,"\u201cé")
						.replace(/"í/g,"\u201cí")
						.replace(/"ó/g,"\u201có")
						.replace(/"ú/g,"\u201cú")
						.replace(/"Á/g,"\u201cÁ")
						.replace(/"É/g,"\u201cÉ")
						.replace(/"Í/g,"\u201cÍ")
						.replace(/"Ó/g,"\u201cÓ")
						.replace(/"Ú/g,"\u201cÚ")
						.replace(/"\[/g,"\u201c[")
						.replace(/"/g, "\u201d")
						.replace(/\b",/g, "\u201d,")
						.replace(/m3/g,'m³')
						.replace(/m2/g,'m²')
						.replace(/\u002E\u002E\u002E/g,'…')
						.replace(/&#8747;/g,'"')
						.replace(/ ­-\b/g, ' \u2013')
						.replace(/\b- /g, '\u2013 ')
						.replace(/\b-, /g, '\u2013, ')
						.replace(/R\$ /g, 'R$\xa0');
		},
		corrige: function(txt) {
			var result = '';

			if(txt.indexOf('<')>=0 && txt.indexOf('>')>=0){
				var text  = txt.split(/\<(.*?)\>/g);

				text.forEach(function(val, i){
					if(i==0){
						result+=arteFolha.utils.cleanUp(val);
					} else if(i!=text.length-1){
						if(arteFolha.utils.isEven(i)){
							result+='>'+arteFolha.utils.cleanUp(val);
						} else {
							result+='<'+val;
						}

					} else {
						result+='>'+arteFolha.utils.cleanUp(val);
					}
				});
			} else {
					result = arteFolha.utils.cleanUp(txt);
			}
			return result;
		},
		saysWho: function(){
			var ua = navigator.userAgent, tem,
			M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
			if(/trident/i.test(M[1])){
				tem =  /\brv[ :]+(\d+)/g.exec(ua) || [];
				return arteFolha.utils.slug('IE '+(tem[1] || ''));
			}
			if(M[1] == 'Chrome'){
				tem = ua.match(/\bOPR\/(\d+)/);
				if(tem != null)
					return arteFolha.utils.slug('Opera '+tem[1]);
			}
			M = M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
			if((tem = ua.match(/version\/(\d+)/i))!= null)
				M.splice(1, 1, tem[1]);

			return arteFolha.utils.slug(M[0]);
		},
		isMobile: function(){
			if(navigator.userAgent.match(/Android/i)||navigator.userAgent.match(/webOS/i)||navigator.userAgent.match(/iPhone/i)||navigator.userAgent.match(/iPad/i)||navigator.userAgent.match(/iPod/i)||navigator.userAgent.match(/BlackBerry/i)||navigator.userAgent.match(/Windows Phone/i)){
				return true;
			}
			else {
				return false;
			}
		},
		slug: function(word) {
			var from = 'ãàáäâẽèéëêìíïîõòóöôùúüûñç·_,:;/-',
				to = 'aaaaaeeeeeiiiiooooouuuunc';
			word = word.toLowerCase().replace(/^\s+|\s+$/g, '').replace(/\s+/g, '').replace('.', '').replace(/'/g, '').replace(/\(/g, '').replace(/\)/g, '').replace(/\[/g, '').replace(/\]/g, '').replace(/\$/g, 's');
			for (var i = 0, l = from.length ; i < l ; i++) {
				word = word.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
			}
			return word;
		},
		getChildren: function(p, c){
			var children = p.childNodes,
				all = [];

			for(i=0; i<children.length; i++){
				if(children[i].classList.contains(c))
					all.push(children[i]);
			}
			return all;
		},
		goRemote: function(url) {
			var script = document.createElement('script');

			script.src=url;
			document.body.appendChild(script);
		},
		shuffle: function(o){
			for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
			return o;
		},
		ripple: function(el, ev){
			$('.ripple').remove();

			el.append('<span class="ripple"></span>');

			$('.ripple').css({
				width:el.parent().width(),
				height:el.parent().width(),
				top:ev.pageY-el.offset().top,
				left: ev.pageX-el.offset().left
			}).addClass('animate');
		}


	}

	function resultado(){
		var i = quizData.perguntas.length/quizData.feedback.length,
			resultado = document.getElementsByClassName('resultado')[0];

		for(key = 0; key < quizData.feedback.length; key++){
			if(score <= i * (key + 1)){
				if(score == 0){
					$('.result-container').html($('.result-container').html().replace('{{score}}', 'Não acertou nenhuma resposta'));
					$('.result-container').html($('.result-container').html().replace('{{feedback}}', quizData.feedback[key].texto));
				} else if(score == 1){
					$('.result-container').html($('.result-container').html().replace('{{score}}', 'Acertou ' + score + ' resposta'));
					$('.result-container').html($('.result-container').html().replace('{{feedback}}', quizData.feedback[key].texto));
				} else {
					$('.result-container').html($('.result-container').html().replace('{{score}}', 'Acertou ' + score + ' respostas'));
					$('.result-container').html($('.result-container').html().replace('{{feedback}}', quizData.feedback[key].texto));
				}

				if(quizData.feedback[key].midia){
					$('.result-img').attr('src', quizData.feedback[key].midia);
				} else {
					$('.result-img').remove();
				}
				break;
			}
		}

		$('.resultado span').on('click', function(e){
			explica($(this), e);
		});
		$('.resultado').on('click', function(e) {
			if(e.target.tagName!='SPAN'){
				$('.tooltip').empty().fadeOut(150);
			}
		});

		/*var i = quizData.perguntas.length/quizData.feedback.length,
			resultado = document.querySelector('.resultado'),
			max = 0,
			scoreLetter;

				var html = "";
				var resp = "";
				if(score === 0 ){
					resp = "Você não acertou nenhuma resposta.";
				}else{
					resp = score === 1 ? "Você acertou 1 resposta." : "Você acertou " + score + " respostas.";
				}


				if( score < 5 ){
					html = (quizData.feedback[0].midia!=undefined && quizData.feedback[0].midia!='' ? '<img src="' + quizData.feedback[0].midia + '" />' : '') + (quizData.feedback[0].texto!=undefined && quizData.feedback[0].texto!='' ? '<h1>' + resp + ' ' +quizData.feedback[0].texto+'</h1>' : '');

				}else if( (score > 4) && (score < 8) ){
					html = (quizData.feedback[1].midia!=undefined && quizData.feedback[1].midia!='' ? '<img src="' + quizData.feedback[1].midia + '" />' : '') + (quizData.feedback[1].texto!=undefined && quizData.feedback[1].texto!='' ? '<h1>' + resp + ' ' +quizData.feedback[1].texto+'</h1>' : '');

				}else{
					html = (quizData.feedback[2].midia!=undefined && quizData.feedback[2].midia!='' ? '<img src="' + quizData.feedback[2].midia + '" />' : '') + (quizData.feedback[2].texto!=undefined && quizData.feedback[2].texto!='' ? '<h1>' + resp + ' ' +quizData.feedback[2].texto+'</h1>' : '');

				}
				$('.result-container').html(html);*/




//
//		score.forEach(function(v, k){
//			if(v > max){
//				max>v;
//				scoreLetter = k;
//			}
//		});
//
//		$('.result-container').html((quizData.feedback[scoreLetter].midia!=undefined && quizData.feedback[scoreLetter].midia!='' ? '<img src="' + quizData.feedback[scoreLetter].midia + '" />' : '') + (quizData.feedback[scoreLetter].texto!=undefined && quizData.feedback[scoreLetter].texto!='' ? '<h1>'+quizData.feedback[scoreLetter].texto+'</h1>' : ''));
	}

	arteFolha.quiz ={
		init: function(data){
			if(arteFolha.utils.isMobile()){
				document.getElementsByTagName('body')[0].classList.add('mobile');
			} else {
				document.getElementsByTagName('body')[0].classList.add('not-mobile');
			}
			$.ajax({
				url: url,
				dataType: 'jsonp',
				jsonpCallback: 'infoBack',
				contentType: 'text/plain',
				success: function(data) {
					quizData = data;
					arteFolha.quiz.load(data);
					var interval=setInterval(function(){
						if(document.readyState=='complete'){
							document.getElementsByClassName('loading')[0].classList.add('fol-hide');
							clearInterval(interval);
						}
					}, 500);
				}
			});
		},
		load: function(data){

			$("#artefolha-quiz .capa-container h1").text(data.titulo);
			$("#artefolha-quiz .capa-container h2").text(data.linhafina);
			if(data.imagem_capa!=undefined && data.imagem_capa!=''){
				$("#artefolha-quiz .capa-container img").attr('src', data.imagem_capa);
			}
			$("#artefolha-quiz .capa-container img").attr('alt', data.titulo);

			var quiz = document.getElementById('quiz'),
				template = '<figure {{classes}} {{id}}>'+
									'<div class="question-container">'+
											/*'<p class="chapeu">{{chapeu}}</p>'+*/
											'{{midia}}'+
											'<figcaption>'+
													'<p>{{pergunta}}</p>'+
											'</figcaption>'+
											'<div class="alternativas">'+
													'{{alternativas}}'+
											'</div>'+
											'<button class="prox active">Continuar'+
												'<div class="seta">'+
													'<div class="haste"></div>'+
													'<div class="ponta"></div>'+
												'</div>'+
											'</button>'+
									'</div>'+
									'<div class="resposta"><div class="resposta-container"><p>{{resposta}}</p><button class="prox">Próximo <div class="seta"><div class="haste"></div><div class="ponta"></div></div></button></div></div>'+
							'</figure>',
				alternativa = '<button class="alternativa active">{{alternativa}}</button>'

			data.perguntas.forEach(function(v, index){
				var slide = template;

				slide = slide.replace('{{id}}', 'id="question'+(index+1)+'"');

				if(index==0){
					slide = slide.replace('{{classes}}', 'class="atual"');
				} else {
					slide = slide.replace('{{classes}}', 'class="proxima"');
				}

				corretas.push(v.alternativas[0].alternativa.replace(' &#8747;'));

				for (var key in v) {
					if(!(v[key] instanceof Array)){
						if(key=='chapeu' && v[key]==''){
							// slide = slide.replace('<p class="chapeu">{{chapeu}}</p>', '');
						} else if(v[key].indexOf('.jpg')<0 && v[key].indexOf('.jpeg')<0 && v[key].indexOf('.gif')<0 && v[key].indexOf('.png')<0){
							slide = slide.replace('{{'+key+'}}', arteFolha.utils.corrige(v[key]));
						} else {
							slide = slide.replace('{{'+key+'}}', '<img src="'+v[key]+'" />');
						}
					} else {
						var alternativas = '',
							alts = arteFolha.utils.shuffle(v[key]);

						alts.forEach(function(val, i){
							if(val.alternativa.indexOf('.jpg')<0 && val.alternativa.indexOf('.jpeg')<0 && val.alternativa.indexOf('.png')<0 && val.alternativa.indexOf('.gif')<0){
								alternativas += alternativa.replace('{{alternativa}}', arteFolha.utils.corrige(val.alternativa));
							} else {
								alternativas += alternativa.replace('{{alternativa}}', '<img src="'+val.alternativa+'"/>').replace('class="', 'class="image ');
							}
						});
						slide = slide.replace('{{alternativas}}', alternativas);
					}
				}
				quiz.innerHTML += slide;
			});
			$('.resposta').hide();
			$('.resultado').hide();
		}
	}

	$(document).on('click', '.alternativa.active', function(e){
			var parent = parseInt(this.parentNode.parentNode.parentNode.id.split('question')[1])-1,
				clicked = $(this).text();

			$('.atual .alternativa').removeClass('active').addClass('disabled');

			$('.atual .alternativa').each(function(){
				if($(this).text() == arteFolha.utils.corrige(corretas[parent])){
						$(this).addClass('certo');
				} else if($(this).text() == clicked){
						$(this).addClass('erro');
				} else {
						$(this).addClass('errado');
				}
			});

			if(clicked == arteFolha.utils.corrige(corretas[parent])){
				score++;
			}

			$('.selected').removeClass('selected');

			$(this).addClass('selected');
			arteFolha.utils.ripple($(this), e);

			if($('.proxima').length>0){
					$('.atual .prox').fadeIn(300);
			} else {
					$('.atual .resposta .prox').html('Concluir <div class="seta"><div class="haste"></div><div class="ponta"></div></div>');
					$('.atual .prox').fadeIn(300);
			}

			$.each(quizData.perguntas[parent].alternativas, function(i, v){
				if(arteFolha.utils.corrige(v.alternativa) == clicked){
					// $('.resposta p').html('<mark>' + v.resposta + '</mark>');
					$('.resposta p').html((quizData.perguntas[parent].chapeu!=undefined && quizData.perguntas[parent].chapeu!='' ? '<span>' + quizData.perguntas[parent].chapeu + '</span>' : '') + (quizData.perguntas[parent].midia_resposta!=undefined && quizData.perguntas[parent].midia_resposta!='' ? '<img src="' + quizData.perguntas[parent].midia_resposta + '" />' : '') + '<mark>' + v.resposta + '</mark>');
				}
			});
			/*setTimeout(function(){
				$('.resposta').fadeIn(300);
			}, 1000);*/
			$('.atual .prox').addClass('fol-show');
			$('.atual').animate({scrollTop: $(window).height()-$('.atual').height()+40}, 150);
	});

	$(document).on('click', 'button', function(e){
		arteFolha.utils.ripple($(this), e);
	});

	$(document).on('click', '.resposta .prox', function(e){
		if($('.proxima').length>0){
			$('.atual').removeClass('atual').addClass('anterior');
			$('.proxima').first().removeClass('proxima').addClass('atual');
			$('.contador span').html(parseInt($('.contador span').html())+1);
			$('.resposta').fadeOut();
		} else {
			$('.resultado').html(resultado()).fadeIn(500);
		}
	});

	$(document).on('click', '.question-container .prox', function(e){
		$('.atual .resposta').fadeIn();
	});

	$('.play').on('click', function(e){
		$('.capa').fadeOut(300);
	});

	arteFolha.quiz.init();

})();