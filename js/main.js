var 
	allGal = [],
	loadedGallery= [],
	touches = [];

var isMobile = function(){
	if(navigator.userAgent.match(/Android/i)||navigator.userAgent.match(/webOS/i)||navigator.userAgent.match(/iPhone/i)||navigator.userAgent.match(/iPad/i)||navigator.userAgent.match(/iPod/i)||navigator.userAgent.match(/BlackBerry/i)||navigator.userAgent.match(/Windows Phone/i)){
		return true;
	}
	else {
		return false;
	}
}

function cleanUp(value){
	return value.replace(/--/g, '\u2013')
				.replace(/\b'\b/g, "\u2019")
				.replace(/'\b/g, "\u2018")
				.replace(/'é/g,"\u2018é")
				.replace(/'É/g,"\u2018É")
				.replace(/'/g, "\u2019")
				.replace(/"\b/g, "\u201c")
				.replace(/"á/g,"\u201cá")
				.replace(/"é/g,"\u201cé")
				.replace(/"í/g,"\u201cí")
				.replace(/"ó/g,"\u201có")
				.replace(/"ú/g,"\u201cú")
				.replace(/"Á/g,"\u201cÁ")
				.replace(/"É/g,"\u201cÉ")
				.replace(/"Í/g,"\u201cÍ")
				.replace(/"Ó/g,"\u201cÓ")
				.replace(/"Ú/g,"\u201cÚ")
				.replace(/"\[/g,"\u201c[")
				.replace(/"/g, "\u201d")
				.replace(/\b",/g, "\u201d,")
				.replace(/m3/g,'m³')
				.replace(/m2/g,'m²')
				.replace(/O2/g,'O₂')
				.replace(/\u002E\u002E\u002E/g,'…')
				.replace(/&#8747;/g,'"');
}

function corrige(txt) {

	var result = ''

	if(txt.search('<')>=0 && txt.search('>')>=0){
		var text  = txt.split(/[<>]/);
		$.each(text, function(i, val){

			if(i==0){
				result+=cleanUp(val);
			} else if(i!=text.length-1){
				if(isEven(i)){
					result+='>'+cleanUp(val);
				} else {
					result+='<'+val;
				}

			} else {
				result+='>'+cleanUp(val);
			}

		});
	} else {
			result = cleanUp(txt);
	}

	return result;
}

function slug(nome) {
	var from = 'ãàáäâẽèéëêìíïîõòóöôùúüûñç·_,:;/-',
		to = 'aaaaaeeeeeiiiiooooouuuunc';

	nome = nome.toLowerCase().replace(/^\s+|\s+$/g, '').replace(/\s+/g, '').replace('.', '').replace(/'/g, '').replace(/\(/g, '').replace(/\)/g, '').replace(/\[/g, '').replace(/\]/g, '').replace(/\$/g, 's');
	for (var i = 0, l = from.length ; i < l ; i++) {
		nome = nome.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
	}
	return nome;
}

function imgLoaded($this){
		
		$this.removeClass('lazy');
}



/* INICIO - Galeria - Botão Direito*/
	function dir(){
		if ($('.mod figure.atual').index()-2>=0) {
			$('.mod').find('figure.atual').removeClass('atual');
			$('.mod').find('figure.proximo').removeClass('proximo').addClass('atual');
			$('.mod').find('figure.enterior').removeClass('anterior').addClass('inativo');
			$('.mod').find('figure.atual').next('figure').removeClass().addClass('proximo');
			$('.mod').find('figure.atual').prev('figure').removeClass().addClass('anterior');
			
			omtrHitCounter('fotos', "img url: "+$('.mod').find('figure.atual img').attr('src'));
			window.ivc('trackPageView');
			
			if(	$('.mod figure.atual').index()-2 >= $('.mod').find('figure').length){
				
				$('#dir	').css({"display": "none"})
			}

			if(	$('.mod figure.atual').index()-2 >= 0){
				
				$('#esq	').css({"display": "block"})
			}
		}
	}
/* FINAL - Galeria - Botão Direito*/


/* INICIO - Galeria - Botão Esquerdo*/
	function esq(){
		$('.mod').find('figure.atual').removeClass('atual');
		$('.mod').find('figure.proximo').removeClass('proximo').addClass('inativo');
		$('.mod').find('figure.anterior').removeClass('anterior').addClass('atual');
		$('.mod').find('figure.atual').prev('figure').removeClass().addClass('anterior');
		$('.mod').find('figure.atual').next('figure').removeClass().addClass('proximo');
		omtrHitCounter('fotos', "img url: "+$('.mod').find('figure.atual img').attr('src'));
		window.ivc('trackPageView');
		if(	$('.mod figure.atual').index()-2 <= $('.mod').find('figure').length){
			$('#dir	').css({"display": "block"})
		}
		if(	$('.mod figure.atual').index()-2 <= 1){
			$('#esq	').css({"display": "none"})

		}
	}
/* FINAL - Galeria - Botão Esquerdo*/


/* INICIO - Galeria - Fechar Galeria*/
	function clos(){
		for (var i = 0; i <  $('.mod').find('figure').length; i++) {
			$('.mod').find('figure').removeClass().addClass('inativo');
		};

		$('.mod').removeClass('mod').addClass('modoff');
		$("#topofolha").css({"z-index":'10000'})


	}
/* FINAL - Galeria - Fechar Galeria*/



/* INICIO - Carrega Conteúdo*/
	function loadData(data){

		var abertura = document.createElement('SECTION'),
			linhaFina = document.createElement('P'),
			content = document.getElementById('content'),
			outras = document.createElement('DIV'),
			moda = document.createElement('DIV'),
			modaImg = document.createElement('IMG'),
			container = document.createElement('SECTION'),
			menu = document.createElement('UL');


			moda.classList.add('moda')
			menu.classList.add('menu')
			modaImg.setAttribute('src','images/moda.svg')

			if (data.capitulos!=undefined && data.capitulos!='') {
				
				for (var i = 0; i < data.capitulos.length; i++) {
					var cap = document.createElement('DIV'),
						capM = document.createElement('LI'),
						capA = document.createElement('A');

						capM.id='mn'+i;
						
						if (data.capitulos[i].titulo.split('<br>').length!=0) {

							if (data.capitulos[i].titulo.split('<br>').length!=0) {
							for (var ii = 0; ii < data.capitulos[i].titulo.split('<br>').length; ii++) {
								var texto =document.createElement('P');
								
								if (ii==0) {
									texto.classList.add('dir')
								}else{
									texto.classList.add('esq')
								}

								texto.innerHTML=data.capitulos[i].titulo.split('<br>')[ii];
								
								capA.setAttribute('href',data.capitulos[i].url )
					
								capA.appendChild(texto)
								capM.appendChild(capA)
							}
						}else{
							var texto =document.createElement('P');
							texto.innerHTML=data.capitulos[i].titulo;
								capM.appendChild(texto)
						}

						}

						cap.classList.add('capitulo');
						container.appendChild(cap)
						cap.id="cap"+i
						cap.style.backgroundImage  = 'url('+data.capitulos[i].foto+')';

					
						menu.appendChild(capM)

				}
							

				
			}

			content.appendChild(menu)
			content.appendChild(container)
$('#cap0').addClass('show')
$('#mn0').addClass('up')

	}
/* FINAL - Carrega Conteúdo*/




/* INTERAÇÃO */

var carr= setInterval(function(){
		if(document.readyState=='complete'){
			var a = parseInt( $('.up').attr('id').split('mn')[1]) + 1

		if (a < $('.menu').find('li').length) {
			$('.up').removeClass('up')
			$('.show').removeClass('show')

			$('#mn'+ a ).addClass('up');
			$('#cap'+ a ).addClass('show');
		}else{
			$('.up').removeClass('up')
			$('.show').removeClass('show')

			$('#mn0' ).addClass('up');
			$('#cap0' ).addClass('show');

		}


		}

		
	}, 3000);
	/* FINAL - INTERAÇÃO - Galeria - Botao Esquerdo */

	/* INICIO - INTERAÇÃO - Galeria - Botao Fechar */
		$(document).on('click ', '.close', function(){
			
			clos()
			$('.info-keys').css({"display":"none"});
			$('body').css({"overflow":"scroll"})
		});
		$(document).on('click ', '.a', function(){
$('#content').css({"margin-left":"-50%"})
		});


		$(document).on( "mouseenter", '.menu li', function(){
			
			$('.up').removeClass('up');
			$('.show').removeClass('show');
			$(this).addClass('up')
			
			$('#cap'+ $(this).attr('id').split('mn')[1]).addClass('show')
clearInterval(carr)

		});

	$(document).on( "mouseleave", '.menu li', function(){
		


	})


	/* FINAL - INTERAÇÃO - Galeria - Botao Fechar */


	$( window ).resize(function() {
		if ($( window ).width()<=650) {
			
			$('.info-keys').css({"display":"none"});
		}

		if ($( window ).width()>=650 && $('#modal2').attr('class') == 'mod' ){

			$('.info-keys').css({"display":"block"});
		};
	})

	document.addEventListener('keydown', function(e){

		if(e.keyCode==39){
			if($('.mod figure.atual').index()-2 < $('.mod figure').length ){
				dir();
			}
		}

		if(e.keyCode==37){
			if($('.mod figure.atual').index()-2>=2){
				esq();
			}

		}
	});






(function(){
	$.ajax({
		url: url,
		dataType: 'jsonp',
		jsonpCallback: 'infoBack',
		contentType: 'text/plain',
		success: function(data) {
			loadData(data);



		}
	});
	var interval=setInterval(function(){
		if(document.readyState=='complete'){
			$('.loading').fadeOut(150);
			
			clearInterval(interval);
		}


	}, 500);


})();