var url = 'json/data2.json',
	allGal = [],
	loadedGallery= [],
	touches = [];

function cleanUp(value){
	return value.replace(/--/g, '\u2013')
				.replace(/\b'\b/g, "\u2019")
				.replace(/'\b/g, "\u2018")
				.replace(/'é/g,"\u2018é")
				.replace(/'É/g,"\u2018É")
				.replace(/'/g, "\u2019")
				.replace(/"\b/g, "\u201c")
				.replace(/"á/g,"\u201cá")
				.replace(/"é/g,"\u201cé")
				.replace(/"í/g,"\u201cí")
				.replace(/"ó/g,"\u201có")
				.replace(/"ú/g,"\u201cú")
				.replace(/"Á/g,"\u201cÁ")
				.replace(/"É/g,"\u201cÉ")
				.replace(/"Í/g,"\u201cÍ")
				.replace(/"Ó/g,"\u201cÓ")
				.replace(/"Ú/g,"\u201cÚ")
				.replace(/"\[/g,"\u201c[")
				.replace(/"/g, "\u201d")
				.replace(/\b",/g, "\u201d,")
				.replace(/m3/g,'m³')
				.replace(/m2/g,'m²')
				.replace(/O2/g,'O₂')
				.replace(/\u002E\u002E\u002E/g,'…')
				.replace(/&#8747;/g,'"');
}

function corrige(txt) {

	var result = ''

	if(txt.search('<')>=0 && txt.search('>')>=0){
		var text  = txt.split(/[<>]/);
		$.each(text, function(i, val){

			if(i==0){
				result+=cleanUp(val);
			} else if(i!=text.length-1){
				if(isEven(i)){
					result+='>'+cleanUp(val);
				} else {
					result+='<'+val;
				}

			} else {
				result+='>'+cleanUp(val);
			}

		});
	} else {
			result = cleanUp(txt);
	}

	return result;
}

function slug(nome) {
	var from = 'ãàáäâẽèéëêìíïîõòóöôùúüûñç·_,:;/-',
		to = 'aaaaaeeeeeiiiiooooouuuunc';

	nome = nome.toLowerCase().replace(/^\s+|\s+$/g, '').replace(/\s+/g, '').replace('.', '').replace(/'/g, '').replace(/\(/g, '').replace(/\)/g, '').replace(/\[/g, '').replace(/\]/g, '').replace(/\$/g, 's');
	for (var i = 0, l = from.length ; i < l ; i++) {
		nome = nome.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
	}
	return nome;
}

function imgLoaded($this){
		
		$this.removeClass('lazy');
}

	function loadData(data){

		var content = document.getElementById('content'),
			outros = document.createElement('DIV'),
			outrosTitle = document.createElement('H2'),
			outrosAll = document.createElement('DIV'),
			outrosCont = document.createElement('DIV'),
			line = document.createElement('hr');

outros.classList.add('outros')
			outrosAll.classList.add('all')
			outrosCont.id='cont'

		outrosTitle.innerHTML= data.titulo;
		outros.appendChild(outrosTitle);
		outros.appendChild(line);
		outros.appendChild(outrosAll);
		outrosAll.appendChild(outrosCont);

		content.appendChild(outros);


		if (data.semanas != '' && data.semanas != undefined && data.semanas[0] != undefined  && data.semanas[0] != '') {

			for (var i = 0; i < data.semanas.length; i++) {
			
			var sem = document.createElement('FIGURE'),
				semLink = document.createElement('A'),
				semImg = document.createElement('IMG'),
				semLeg = document.createElement('FIGCAPTION');
			

				semLink.setAttribute('href', data.semanas[i].url);

				semImg .setAttribute('src', data.semanas[i].img);

				semLeg.innerHTML= '<b>'+ data.semanas[i].data +'</b> <br>'+data.semanas[i].legenda;
				

			sem.appendChild(semLink);
			semLink.appendChild(semImg);
			semLink.appendChild(semLeg);

			outrosCont.appendChild(sem);


			}

			$('#cont').css({'width':"calc( 20vw * "+data.semanas.length +")"})


		}



	}






(function(){
	$.ajax({
		url: url,
		dataType: 'jsonp',
		jsonpCallback: 'infoBack',
		contentType: 'text/plain',
		success: function(data) {
			loadData(data);



		}
	});
	var interval=setInterval(function(){
		if(document.readyState=='complete'){
			$('.loading').fadeOut(150);
			
			clearInterval(interval);
		}
	}, 500);
})();