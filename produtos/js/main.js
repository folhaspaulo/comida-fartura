var dados = {},
  touches = [],
  lazyStart = false,
  imgBlank = 'data:image/gif;base64,R0lGODlhCgAKAIAAAB8fHwAAACH5BAEAAAAALAAAAAAKAAoAAAIIhI+py+0PYysAOw==',
  changeHash = false,
  s;

window.onresize = function() {
  artefolha.sobrinho.resize();
};

var artefolha = artefolha || {};

artefolha.utils = {
  isEven: function(n) {
    return (n % 2 === 0);
  },
  cleanUp: function(value) {
    return value.replace(/--/g, '\u2013')
      .replace(/\b'\b/g, "\u2019")
      .replace(/'\b/g, "\u2018")
      .replace(/'/g, "\u2019")
      .replace(/"\b/g, "\u201c")
      .replace(/"Ã¢/g, "\u201cÃ¢")
      .replace(/"Ãª/g, "\u201cÃª")
      .replace(/"Ã´/g, "\u201cÃ´")
      .replace(/"Ã£/g, "\u201cÃ£")
      .replace(/"Ãµ/g, "\u201cÃµ")
      .replace(/"Ã/g, "\u201cÃ")
      .replace(/"Ã‰/g, "\u201cÃ‰")
      .replace(/"Ã/g, "\u201cÃ")
      .replace(/"Ã“/g, "\u201cÃ“")
      .replace(/"Ãš/g, "\u201cÃš")
      .replace(/"Ã¡/g, "\u201cÃ¡")
      .replace(/"Ã©/g, "\u201cÃ©")
      .replace(/"Ã­/g, "\u201cÃ­")
      .replace(/"Ã³/g, "\u201cÃ³")
      .replace(/"Ãº/g, "\u201cÃº")
      .replace(/"Ã/g, "\u201cÃ")
      .replace(/"Ã‰/g, "\u201cÃ‰")
      .replace(/"Ã/g, "\u201cÃ")
      .replace(/"Ã“/g, "\u201cÃ“")
      .replace(/"Ãš/g, "\u201cÃš")
      .replace(/"\[/g, "\u201c[")
      .replace(/"/g, "\u201d")
      .replace(/\b",/g, "\u201d,")
      .replace(/m3/g, 'mÂ³')
      .replace(/m2/g, 'mÂ²')
      .replace(/\u002E\u002E\u002E/g, 'â€¦')
      .replace(/&#8747;/g, '"')
      .replace(/ Â­-\b/g, ' \u2013')
      .replace(/\b- /g, '\u2013 ')
      .replace(/\b-, /g, '\u2013, ')
      .replace(/R\$ /g, 'R$\xa0');
  },
  corrige: function(txt) {
    var result = '';

    if (txt.indexOf('<') >= 0 && txt.indexOf('>') >= 0) {
      var text = txt.split(/\<(.*?)\>/g);

      text.forEach(function(val, i) {
        if (i == 0) {
          result += artefolha.utils.cleanUp(val);
        } else if (i != text.length - 1) {
          if (artefolha.utils.isEven(i)) {
            result += '>' + artefolha.utils.cleanUp(val);
          } else {
            result += '<' + val;
          }

        } else {
          result += '>' + artefolha.utils.cleanUp(val);
        }
      });
    } else {
      result = artefolha.utils.cleanUp(txt);
    }
    return result;
  },
  saysWho: function() {
    var ua = navigator.userAgent,
      tem,
      M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
      tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
      return artefolha.utils.slug('IE ' + (tem[1] || ''));
    }
    if (M[1] == 'Chrome') {
      tem = ua.match(/\bOPR\/(\d+)/);
      if (tem != null)
        return artefolha.utils.slug('Opera ' + tem[1]);
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null)
      M.splice(1, 1, tem[1]);

    return artefolha.utils.slug(M[0]);
  },
  isMobile: function() {
    if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
      return true;
    } else {
      return false;
    }
  },
  slug: function(word) {
    var from = 'Ã£Ã Ã¡Ã¤Ã¢áº½Ã¨Ã©Ã«ÃªÃ¬Ã­Ã¯Ã®ÃµÃ²Ã³Ã¶Ã´Ã¹ÃºÃ¼Ã»Ã±Ã§Â·_,:;/-',
      to = 'aaaaaeeeeeiiiiooooouuuunc';

    word = word.toLowerCase().replace(/^\s+|\s+$/g, '').replace(/\s+/g, '').replace('.', '').replace(/'/g, '').replace(/\(/g, '').replace(/\)/g, '').replace(/\[/g, '').replace(/\]/g, '').replace(/\$/g, 's');
    for (var i = 0, l = from.length; i < l; i++) {
      word = word.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }
    return word;
  },
  getChildren: function(p, c) {
    var children = p.childNodes,
      all = [];

    for (i = 0; i < children.length; i++) {
      if (children[i].classList.contains(c))
        all.push(children[i]);
    }
    return all;
  },
  goRemote: function(url) {
    var script = document.createElement('script');

    script.src = url;
    document.body.appendChild(script);
  },
  scrollHash: function() {
    if (window.location.hash || window.location.hash !== '') {

      var hash = window.location.hash,
        aTag = $(hash);


        if (!changeHash) {
          changeHash = true;
          $('html,body').delay(100).animate({
            scrollTop: aTag.offset().top - 180
          }, 0);
        }
    }
  }
};

artefolha.sobrinho = {
  load: function(data) {
    dados = data;
  var abertura = document.createElement('SECTION'),
      linhaFina = document.createElement('P'),
      content = document.getElementById('content'),
      outras = document.createElement('DIV'),
      container = document.createElement('SECTION'),
      title = document.createElement('H1'),
      titleIMG = document.createElement('img');

      titleIMG.classList.add("bot")
      title.innerHTML = data.titulo_especial;

      titleIMG.setAttribute("src","images/bot.svg")

      content.appendChild(title)
      content.appendChild(titleIMG)



    data.noticias.forEach(function(val, i) {

      if (data.noticias[i].imagem_topo != undefined && data.noticias[i].imagem_topo != '') {
        artefolha.sobrinho.fullImg(data.noticias[i], i);
      }

      paragrafos = data.noticias[i].texto.split('\n\n');

      var txt = document.createElement('section');

      txt.classList.add('texto');
      txt.id = 'texto' + (i + 1);

      if (i == 0 && data.linha_fina != undefined && data.linha_fina != '') {
        var linhaFina = document.createElement('h3');

        linhaFina.innerHTML = artefolha.utils.corrige(data.linha_fina);
        txt.appendChild(linhaFina);
      }

      document.querySelector('article').appendChild(txt);

      if (data.noticias[i].titulo != undefined && data.noticias[i].titulo != '') {
        var h1 = document.createElement('h2');

        h1.innerHTML = artefolha.utils.corrige(data.noticias[i].titulo);
        document.getElementById('texto' + (i + 1)).appendChild(h1);
      }

      if (data.noticias[i].autor != undefined && data.noticias[i].autor != '') {
        var autor = document.createElement('p');
        autor.classList.add('autor');
        autor.innerHTML = artefolha.utils.corrige(data.noticias[i].autor);
        if (data.noticias[i].origem != undefined && data.noticias[i].origem != '') {
          var origem = document.createElement('span');
          origem.innerHTML = artefolha.utils.corrige(data.noticias[i].origem);
          autor.appendChild(origem);
        }
        document.getElementById('texto' + (i + 1)).appendChild(autor);
      }
      if (data.noticias[i].date != undefined && data.noticias[i].date != '') {
        var date = document.createElement('p');

        date.classList.add('date');
        date.appendChild(document.createTextNode(data.noticias[i].date));
        document.getElementById('texto' + (i + 1)).appendChild(date);
      }

      for (index = 0; index < paragrafos.length; index++) {
        if (paragrafos[index].indexOf('{{') == 0) {
          var deleteClass = document.createElement('p');

          if (paragrafos[index].indexOf('video') > -1 && data.noticias[i].video[0].codigoUol != undefined && data.noticias[i].video[0].codigoUol != '') {
            deleteClass.classList.add('delete-uol');
          } else {
            deleteClass.classList.add('delete');
          }

          deleteClass.appendChild(document.createTextNode(paragrafos[index]));
          document.getElementById('texto' + (i + 1)).appendChild(deleteClass);
        } else {
          if (paragrafos[index].search('h1') > 0 || paragrafos[index].search('!--') > 0 || paragrafos[index].search('h2') > 0) {
            document.getElementById('texto' + (i + 1)).innerHTML += artefolha.utils.corrige(paragrafos[index]);
          } else {
            var p = document.createElement('p');

            p.innerHTML = artefolha.utils.corrige(paragrafos[index]);
            document.getElementById('texto' + (i + 1)).appendChild(p);
          }
        }
      }
    });


      artefolha.sobrinho.loadObj();



          document.addEventListener("touchstart", function(e){

                console.log('start: '+ e.changedTouches[0].pageY);
                starty = parseInt( e.changedTouches[0].pageY)


         });

          document.addEventListener("touchend", function(e){

            if ($('.gal-opn').length >=1) {

            if (e.changedTouches[0].pageY <= starty) {
              $('.anterior').removeClass('anterior')
              $('.gal-opn').removeClass('gal-opn').addClass('anterior')
              $('.proximo').removeClass('proximo').addClass('gal-opn')
    
              if ($('.gal-opn').length ==0) {
                $('.closee').css({"display":"none"})
                $('.dir1').css({"display":"none"})
                $('.esq1').css({"display":"none"})
              };
              
            var aaaa = parseInt($('.gal-opn').attr('id').split('gal')[1]);
            $('#gal'+(aaaa+1)).addClass('proximo')
            var aaaa = parseInt($('.gal-opn').attr('id').split('gal')[1]);
            $('#gal'+(aaaa-1)).addClass('anterior')
            $('.dir1').css({"display":"block"})
            $('.esq1').css({"display":"block"})

            if( parseInt($('.gal-opn').attr('id').split('gal')[1]) == 1 ) {
              $('.dir1').css({"display":"block"})
              $('.esq1').css({"display":"none"})
           
            };

            if (parseInt($('.gal-opn').attr('id').split('gal')[1])  ==  $('.gal').length ) {
              $('.dir1').css({"display":"none"})
              $('.esq1').css({"display":"block"})
            
            };
            omtrHitCounter('fotos', "imagem" + $('.gal-opn').attr('id').split('gal')[1]);
  window.ivc('trackPageView');

            }

            if (e.changedTouches[0].pageY >= starty ) {
              $('.proximo').removeClass('proximo')
              $('.gal-opn').removeClass('gal-opn').addClass('proximo')
              $('.anterior').removeClass('anterior').addClass('gal-opn')
              

              if ($('.gal-opn').length ==0) {
                $('.closee').css({"display":"none"})
                $('.dir1').css({"display":"none"})
                $('.esq1').css({"display":"none"})
              };

              var aaaa = parseInt($('.gal-opn').attr('id').split('gal')[1]);
                $('#gal'+(aaaa-1)).addClass('anterior')

                $('.dir1').css({"display":"block"})
                $('.esq1').css({"display":"block"})

               if ( parseInt($('.gal-opn').attr('id').split('gal')[1]) == 1 ) {
                $('.dir1').css({"display":"block"})
                $('.esq1').css({"display":"none"})

              };

                if (parseInt($('.gal-opn').attr('id').split('gal')[1])  ==  $('.gal').length ) {
                $('.dir1').css({"display":"none"})
                $('.esq1').css({"display":"block"})
                };
                            omtrHitCounter('fotos', "imagem" + $('.gal-opn').attr('id').split('gal')[1]);
  window.ivc('trackPageView');
            }

            if (e.changedTouches[0].pageY == starty ){


            if ($('.mobile .gal figcaption').css('opacity') == 0) {
              $('.mobile .gal figcaption').css({"opacity":"1"})
}else{

   $('.mobile .gal-opn figcaption').css({"opacity":"0"})
}
            }

              
};



         });






  $('.esq1').on( 'click',  function(){
        $('.proximo').removeClass('proximo')
    $('.gal-opn').removeClass('gal-opn').addClass('proximo')
    $('.anterior').removeClass('anterior').addClass('gal-opn')
    
    if ($('.gal-opn').length == 0) {

      $('.closee').css({"display":"none"})
      $('.dir1').css({"display":"none"})
      $('.esq1').css({"display":"none"})

    };
    
    var aaaa = parseInt($('.gal-opn').attr('id').split('gal')[1]);
       $('#gal'+(aaaa-1)).addClass('anterior')



      $('.dir1').css({"display":"block"})
        $('.esq1').css({"display":"block"})

               if ( parseInt($('.gal-opn').attr('id').split('gal')[1]) == 1 ) {
        $('.dir1').css({"display":"block"})
        $('.esq1').css({"display":"none"})

        };

        if (parseInt($('.gal-opn').attr('id').split('gal')[1])  ==  $('.gal').length ) {
        $('.dir1').css({"display":"none"})
        $('.esq1').css({"display":"block"})
        };

            omtrHitCounter('fotos', "imagem" + $('.gal-opn').attr('id').split('gal')[1]);
  window.ivc('trackPageView');


  })


  $('.dir1').on( 'click',  function(){
    $('.anterior').removeClass('anterior')
    $('.gal-opn').removeClass('gal-opn').addClass('anterior')
    $('.proximo').removeClass('proximo').addClass('gal-opn')
    
    if ($('.gal-opn').length ==0) {
      $('.closee').css({"display":"none"})
      $('.dir1').css({"display":"none"})
      $('.esq1').css({"display":"none"})
    };
    
    var aaaa = parseInt($('.gal-opn').attr('id').split('gal')[1]);
      $('#gal'+(aaaa+1)).addClass('proximo')
          var aaaa = parseInt($('.gal-opn').attr('id').split('gal')[1]);
       $('#gal'+(aaaa-1)).addClass('anterior')

 $('.dir1').css({"display":"block"})
        $('.esq1').css({"display":"block"})

               if ( parseInt($('.gal-opn').attr('id').split('gal')[1]) == 1 ) {
        $('.dir1').css({"display":"block"})
        $('.esq1').css({"display":"none"})

        };

        if (parseInt($('.gal-opn').attr('id').split('gal')[1])  ==  $('.gal').length ) {
        $('.dir1').css({"display":"none"})
        $('.esq1').css({"display":"block"})
        };

            omtrHitCounter('fotos', "imagem" + $('.gal-opn').attr('id').split('gal')[1]);
  window.ivc('trackPageView');
  })

    //expediente
    artefolha.sobrinho.expediente("Revisão de Texto: Bruno Lee e Gustavo Simon/ Design e Desenvolvimento: Thiago Almeida");
    artefolha.sobrinho.resize();

  },
  resize: function() {
    var ww = window.innerWidth,
      wh = window.innerHeight;

    if (!Modernizr.cssvhunit) {
      //document.getElementById('full1').style.height = wh/2+'px';
      $('#full1').height(wh / 2 + 'px');
      var videos = $('.video');

      for (i = 0; i < videos.length; i++) {
        if (ww < 940) {
          videos[i].style.height = ww * 9 / 16 + 'px';
        } else {
          videos[i].style.height = '528px';
        }
      }
    }

    if (!artefolha.utils.isMobile()) {
      var galerias = document.getElementsByClassName('galeria');

      for (index = 0; index < galerias.length; index++) {
        for (k = 0; k < galerias[index].childNodes.length; k++) {
          if (galerias[index].childNodes[k].classList.contains('atual')) {
            galerias[index].style.height = galerias[index].childNodes[k].clientHeight + 'px';
          }
        }
      }
    }

    if (ww < 1500 && ww / wh / 2 <= 0.5) {
      $('#full1 .center').height(wh / 2);
      $('#full1 .center').css({
        'width': 'auto'
      });
    } else if (ww < 1500 && ww / wh / 2 > 0.5) {
      $('#full1 .center').width(ww);
      $('#full1 .center').css({
        'height': 'auto'
      });
    } else {
      $('#full1 .center').css({
        'width': 'auto',
        'height': 'auto'
      });
    }

   
        $('#slide1 .slider').removeClass('animate').css('left',$('#slide1').width()/2+'px');
        $('#slide1 .antes').removeClass('animate').css('width',$('#slide1').width()/2+'px');

  },
  loadObj: function() {
    var paragraphs = document.querySelectorAll('.delete'),
      paraUol = document.querySelectorAll('.delete-uol'),
      idx = paragraphs.length;

    for (key = 0; key < idx; key++) {
      if (paragraphs[key] != undefined) {
        if (paragraphs[key].textContent.indexOf('aspa') > -1) {
          // ADD ASPAS
          artefolha.sobrinho.aspa(paragraphs[key]);
        } else if (paragraphs[key].textContent.indexOf('info') >= 0) {
          // ADD INFOGRAFICOS
          artefolha.sobrinho.infos(paragraphs[key]);
                   
        } else if (paragraphs[key].textContent.indexOf('video') > -1) {
          // ADD VIDEOS
          artefolha.sobrinho.video(paragraphs[key]);
        } else if (paragraphs[key].textContent.indexOf('galeria') >= 0) {
          // ADD GALERIAS
          artefolha.sobrinho.galerias(paragraphs[key], key);
        } else {
          paragraphs[key].parentNode.removeChild(paragraphs[key]);
        }
        artefolha.sobrinho.resize();
      }
    }

    var ajaxQueue = $({});

    $.ajaxQueue = function(ajaxOpts) {
      var oldComplete = ajaxOpts.complete;

      ajaxQueue.queue(function(next) {
        ajaxOpts.complete = function() {
          if (oldComplete) {
            oldComplete.apply(this, arguments);
          }
          next();
        };
        $.ajax(ajaxOpts);
      });
    };

    $.each(paraUol, function(i, v) {
      uol = dados.noticias[parseInt(v.parentNode.id.replace('texto', '')) - 1].video[parseInt(v.innerHTML.replace('{{video=', '').replace('}}', '')) - 1].codigoUol.split('view/')[1];
      $.ajaxQueue({
        async: true,
        type: 'GET',
        url: 'http://mais.uol.com.br/apiuol/player/media?mediaId=' + uol,
        dataType: 'jsonp',
        jsonpCallback: "media_data",
        success: function(data) {
          artefolha.sobrinho.video(paraUol[i], data.media.thumbnail);
        }
      });
    });

  },
  aspa: function(paragraph) {
    var thisId = parseInt(paragraph.textContent.split('=')[1].replace('}}', '')) - 1,
      aspa = dados.noticias[(parseInt(paragraph.parentNode.id.replace('texto', '')) - 1)].aspas[thisId];

    if (aspa != undefined && aspa.citacao != undefined && aspa.citacao != '') {
      var quote = document.createElement('blockquote'),
        quoteP = document.createElement('p'),
        cite = document.createElement('cite'),
        shareQ = document.createElement('div'),
        shareA = document.createElement('a');

      shareA.setAttribute('href', 'https://twitter.com/home?status=' + encodeURIComponent('â€œ' + aspa.citacao + 'â€' + (aspa.quem != undefined && aspa.quem != null && aspa.quem != '' ? ' ' + aspa.quem : '') + ' ' + window.location.href + ' @folha'));
      shareA.setAttribute('target', '_blank');
      shareA.appendChild(document.createTextNode('Compartilhar'));
      shareQ.appendChild(shareA);
      shareQ.classList.add('share-quote');
      quote.appendChild(shareQ);
      quoteP.appendChild(document.createTextNode(artefolha.utils.corrige(aspa.citacao)));
      quote.appendChild(quoteP);
      if (aspa.quem != undefined && aspa.quem != '') {
        cite.appendChild(document.createTextNode(artefolha.utils.corrige(aspa.quem)));
        quote.appendChild(cite);
        if (aspa.cargo != undefined && aspa.cargo != '') {
          var span = document.createElement('span');
          span.appendChild(document.createTextNode(', ' + artefolha.utils.corrige(aspa.cargo)));
          cite.appendChild(span);
        }
      }
      paragraph.parentNode.replaceChild(quote, paragraph);
    } else {
      paragraph.parentNode.removeChild(paragraph);
    }
  },
  infos: function(paragraph) {
    var thisId = parseInt(paragraph.textContent.split('=')[1].replace('}}', '')) - 1,
      info = dados.noticias[parseInt(paragraph.parentNode.id.replace('texto', '')) - 1].info[thisId];



    if (info.url != undefined && info.url != '') {
      var infografico = document.createElement('div');

      if (info.id != undefined && info.id != '') {
        var image = document.createElement('iframe');



        image.src = info.url;

        infografico.appendChild(image);

        if (info.id.split('&').length > 1) {

          var title = document.createElement('h4'),
            text = document.createElement('p');

          title.appendChild(document.createTextNode(info.id.split('&')[0]));
          text.appendChild(document.createTextNode(info.id.split('&')[1]));

          infografico.appendChild(title);
          infografico.appendChild(text);
        }
        infografico.classList.add('info', 'embed');

      } else if (info.url.indexOf('.jpeg') > -1 || info.url.indexOf('.jpg') > -1 || info.url.indexOf('.gif') > -1 || info.url.indexOf('.png') > -1) {
        var img = document.createElement('img');

        img.setAttribute('src', info.url);
        infografico.classList.add('info');
        infografico.classList.add('lado');
        infografico.appendChild(img);
      } else {
        var iframe = document.createElement('iframe'),
          sizes = info.url.split(/\?(?=[^?]*$)/)[1].split(/\&/g);

        sizes.forEach(function(val) {
          if (val.indexOf('w') > -1) {
            iframe.width = val.split('=')[1];
          } else if (val.indexOf('h') > -1) {
            iframe.height = val.split('=')[1];
          }
        });
        iframe.setAttribute('frameborder', 0);
        iframe.setAttribute('src', info.url.split(/\?(?=[^?]*$)/)[0]);
        infografico.classList.add('info', 'embed');
        infografico.appendChild(iframe);
      }
      paragraph.parentNode.replaceChild(infografico, paragraph);
    }
  },
  video: function(paragraph, thumb) {
    var thisId = parseInt(paragraph.textContent.split('=')[1].replace('}}', '')) - 1,
      video,
      vid = document.createElement('div');


    if (dados.noticias[parseInt(paragraph.parentNode.id.replace('texto', '')) - 1].video != undefined && dados.noticias[parseInt(paragraph.parentNode.id.replace('texto', '')) - 1].video.length > 0) {
      video = dados.noticias[parseInt(paragraph.parentNode.id.replace('texto', '')) - 1].video[thisId];

      if (video.codigoUol != undefined && video.codigoUol != '') {
        var uol = document.createElement('div'),
          uolCode = video.codigoUol.indexOf('view/') > 0 ? video.codigoUol.split('view/')[1] : video.codigoUol;

        // if(!artefolha.utils.isMobile()){
        var play = document.createElement('div');

        uol.classList.add('vplay');
        play.classList.add('play');

        uol.appendChild(play);

        vid.classList.add('video');

        if (thumb != undefined) {
          uol.style.backgroundImage = 'url(' + thumb + ')';
        }

        vid.appendChild(uol);
        vid.dataset.uol = uolCode;
        vid.id = 'video' + thisId;
        paragraph.parentNode.replaceChild(vid, paragraph);

        uol.addEventListener('click', function() {
          this.parentNode.innerHTML = '<iframe width="640" height="360" src="http://mais.uol.com.br/static/uolplayer/?autoplay=true&mediaId=' + this.parentNode.dataset.uol + '" allowfullscreen frameborder="0"></iframe>';
        }, false);



        /*} else {
        	vid.innerHTML = '<iframe width="640" height="360" src="http://mais.uol.com.br/static/uolplayer/?autoplay=true&mediaId='+uolCode+'" allowfullscreen frameborder="0"></iframe>';

        	vid.id = 'video'+thisId;
        	vid.classList.add('video');

        	paragraph.parentNode.replaceChild(vid, paragraph);
        }*/

      } else if (video.codigoYoutube != undefined && video.codigoYoutube != '') {
        var youtube = document.createElement('div'),
          ytCode = video.codigoYoutube.indexOf('?v=') > 0 ? video.codigoYoutube.split('?v=')[1] : video.codigoYoutube,
          play = document.createElement('div');

        youtube.width = 940;
        youtube.height = 528;
        youtube.classList.add('vplay');
        play.classList.add('play');
        youtube.appendChild(play);
        youtube.style.backgroundImage = 'url(http://img.youtube.com/vi/' + ytCode + '/maxresdefault.jpg)';

        vid.appendChild(youtube);
        vid.classList.add('video');
        vid.dataset.youtube = ytCode;
        vid.id = 'video' + thisId;
        paragraph.parentNode.replaceChild(vid, paragraph);
      }
    } else {
      paragraph.parentNode.removeChild(paragraph);
    }
  },
  galerias: function(paragraph, id) {
       var galeria = dados.noticias[parseInt(paragraph.parentNode.id.replace('texto', '')) - 1].fotos,
      gal = document.createElement('div'),
      galContainer = document.createElement('div'),
      dirBtn = document.createElement('button'),
      esqBtn = document.createElement('button');

    gal.classList.add('galeria');
    gal.id = 'galeria' + id;


    if(galeria[0].url!=undefined) {
    console.log(galeria[0]);
        $.ajax({
          type: 'GET',
          url: galeria[0].url + '.jsonp',
          dataType: 'jsonp',
          jsonpCallback: "fotografia_callback",
          success: function(data) {
        
            data.images.forEach( function(val, key) {
              var slide = document.createElement('figure'),
                imgContainer = document.createElement('div'),
                img = document.createElement('img');

              if (key == 0) {
                img.src = val.image_gallery;
                slide.classList.add('atual');
                slide.dataset.src = val.image_gallery;
                img.setAttribute('onload', 'artefolha.sobrinho.unlazy($(this));');
              } else {
                slide.dataset.src = val.image_gallery;
                img.setAttribute('src', '#');
                slide.classList.add('lazy');
                slide.classList.add('proximo');
                img.setAttribute('onload', 'artefolha.sobrinho.unlazy($(this));');
              }
              imgContainer.classList.add('gal-img-container');
              imgContainer.appendChild(img);

              slide.appendChild(imgContainer);

              if (val.legend_javascript != undefined && val.legend_javascript != '' || val.credit != undefined && val.credit != '') {
                var caption = document.createElement('figcaption'),
                  legenda = '';

                caption.innerHTML = '<h1>' + data.gallery.title + '</h1>';

                legenda = '<p>';

                if (val.legend_javascript != undefined && val.legend_javascript != ''){
                  legenda += artefolha.utils.corrige(val.legend_javascript);
                }

                if (val.credit != undefined && val.credit != '') {

                  if(val.legend_javascript != undefined && val.legend_javascript != ''){
                    legenda += ' ';
                  }

                  legenda += '(' + val.credit + ')';
                }

                legenda += '<p class="contador">' + (key+1) + ' de ' + data.images.length + '</p>';

                caption.innerHTML += legenda;

                slide.appendChild(caption);
              }
              galContainer.appendChild(slide);
            });
            galContainer.classList.add('gal-container');

            galContainer.dataset.anchor = 'fotos-dilma';

            gal.appendChild(galContainer);

            if (data.images.length > 1) {
              dirBtn.classList.add('dir');
              esqBtn.classList.add('esq');
              dirBtn.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 60 60"><defs><polygon id="dir-btn-icon" points="45.121,30.868 27.282,13.029 23.04,17.271 36.637,30.868 23.04,44.465 27.282,48.707 "/></defs><use stroke="#000" stroke-width="3  " stroke-linecap="round" stroke-linejoin="round" fill="none" opacity=".3" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#dir-btn-icon"></use><use fill="#fff" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#dir-btn-icon"></use></svg>';
              esqBtn.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 60 60"><defs><polygon id="esq-btn-icon" points="14.879,29.132 32.718,46.971 36.96,42.729 23.363,29.132 36.96,15.535 32.718,11.293 "/></defs><use stroke="#000" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" fill="none" opacity=".3" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#esq-btn-icon"></use><use fill="#fff" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#esq-btn-icon"></use></svg>';
              esqBtn.style.display = 'none';

              gal.appendChild(dirBtn);
              gal.appendChild(esqBtn);
            }
            paragraph.parentNode.replaceChild(gal, paragraph);

            $('#galeria' + id).prepend('<header><button class="fs-gallery"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><defs><path id="fs-icon" d="M7 14H5v5h5v-2H7v-3zm-2-4h2V7h3V5H5v5zm12 7h-3v2h5v-5h-2v3zM14 5v2h3v3h2V5h-5z"/><path id="fs-close" d="M5 16h3v3h2v-5H5v2zm3-8H5v2h5V5H8v3zm6 11h2v-3h3v-2h-5v5zm2-11V5h-2v5h5V8h-3z"/></defs><use class="fs-open" stroke="#000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" fill="none" opacity=".3" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#fs-icon"></use><use class="fs-open white" fill="#fff" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#fs-icon"></use><use stroke="#000" stroke-width="2 "stroke-linecap="round" stroke-linejoin="round" fill="none" opacity=".3" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#fs-close" class="fs-close"></use><use fill="#000" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#fs-close" class="fs-close"></use></svg></button></header>');

            artefolha.sobrinho.resize();

            galContainer.addEventListener('touchstart', function(e) {
              touches.push(e.touches[0].clientX);
            });

            galContainer.addEventListener('touchend', function(e) {
              touches.push(e.changedTouches[0].clientX);
              if ((touches[touches.length - 2] - touches[touches.length - 1]) > 100) {
                artefolha.sobrinho.dir($(this).parent().parent().parent().parent());
              } else if ((touches[touches.length - 2] - touches[touches.length - 1]) < -100) {
                artefolha.sobrinho.esq($(this).parent().parent().parent().parent());
              }
              touches = [];
            });
          }
        });
};
  },
  galeriaMobile: function(paragraph) {
    var galeria = dados.noticias[parseInt(paragraph.parentNode.id.replace('texto', '')) - 1].fotos,
      gal = document.createElement('div'),
      dirBtn = document.createElement('button'),
      esqBtn = document.createElement('button'),
      id = paragraph.parentNode.id.replace('texto', '');

    gal.classList.add('galeria');
    gal.id = 'galeria' + id;

    if (galeria[0].url != undefined && galeria[0].url != '') {
      var slide1 = document.createElement('figure'),
        img1 = document.createElement('img');

      img1.setAttribute('src', galeria[0].url);
      img1.setAttribute('onload', 'artefolha.sobrinho.resize();');
      slide1.appendChild(img1);

      if (galeria[0].legenda != undefined && galeria[0].legenda != '' || galeria[0].credito != undefined && galeria[0].credito != '') {
        var legenda1 = document.createElement('figcaption');

        if (galeria[0].legenda != undefined && galeria[0].legenda != '') {
          legenda1.innerHTML = artefolha.utils.corrige(galeria[0].legenda);
        }
        if (galeria[0].credito != undefined && galeria[0].credito != '') {
          var credito1 = document.createElement('span');

          credito1.appendChild(document.createTextNode(galeria[0].credito));
          legenda1.appendChild(credito1);

        }
        if (galeria[0].url != undefined && galeria[0].url != '') {
          var overlay = document.createElement('div');

          slide1.classList.add('gal-open');
          slide1.classList.add('opener');

          overlay.id = 'overlay' + id;
          overlay.classList.add('overlay');

          for (i = 0; i < galeria.length; i++) {
            if (galeria[i].url != undefined && galeria[i].url != '') {
              var figure = document.createElement('figure'),
                img = document.createElement('img');

              if (i == 0) {
                figure.classList.add('atual');
              } else {
                figure.classList.add('proximo');
              }

              img.setAttribute('src', galeria[i].url);
              figure.appendChild(img);

              if (galeria[i].legenda != undefined && galeria[i].legenda != '' || galeria[i].credito != undefined && galeria[i].credito != '') {
                var caption = document.createElement('figcaption'),
                  legenda = document.createElement('p');

                if (galeria[i].legenda != undefined && galeria[i].legenda != '')
                  legenda.innerHTML = galeria[i].legenda;
                if (galeria[i].credito != undefined && galeria[i].credito != '') {
                  var credito = document.createElement('span');

                  credito.appendChild(document.createTextNode(galeria[i].credito));
                  legenda.appendChild(credito);
                }

                caption.appendChild(legenda);
                figure.appendChild(caption);
              }
            }
            overlay.appendChild(figure);


          }
          var close = document.createElement('button');

          close.classList.add('close');
          overlay.appendChild(close);

          dirBtn.classList.add('dir');
          overlay.appendChild(dirBtn);

          esqBtn.classList.add('esq');
          overlay.appendChild(esqBtn);

          gal.appendChild(overlay);
        }
        slide1.appendChild(legenda1);
      }

      gal.appendChild(slide1);

    } else {
      if (paragraphs != undefined) {
        document.getElementById('texto' + id).removeChild(paragraphs[0]);
      }
    }
    paragraph.parentNode.replaceChild(gal, paragraph);
    artefolha.sobrinho.dynamicClicks(esqBtn, dirBtn, gal);
  },
  fullImg: function(data, i) {
    if (data.imagem_topo != undefined && data.imagem_topo != '' && data.imagem_topo.indexOf('videos') < 0) {
      var images = [];
      gallery = document.createElement('section');

      gallery.classList.add('gal');
      gallery.id = 'gal' +i;

      if (data.imagem_topo.split('&').length > 1) {
        images = data.imagem_topo.split('&');
      } else {
        images.push(data.imagem_topo);
      }







  $('.closee').on( 'click',  function(){
    $('.closee').css({"display":"none"})
    $('.gal-opn').removeClass('gal-opn').addClass('gal')
    $('.dir1').css({"display":"none"})
    $('.esq1').css({"display":"none"})
    omtrHitCounter('fotos', "fechou");
  window.ivc('trackPageView');

})


      images.forEach(function(v, key) {
        var foto = document.createElement('figure'),
          img = document.createElement('img');

        if (key == 0 && images.length > 1) {
          foto.classList.add('atual');
          foto.style.position = 'relative';
        } else if (images.length > 1) {
          foto.classList.add('proximo');
        } else {
          foto.classList.add('single');
        }

        img.setAttribute('src', v);

        foto.appendChild(img);

        if (data.legenda_foto != undefined && data.legenda_foto != '') {
          var legendaTopo = document.createElement('figcaption');

          legendaTopo.innerHTML = data.legenda_foto.split('&')[key] + '<span>' + data.credito_foto + '</span>';

          foto.appendChild(legendaTopo);
        }

        if (data.credito_foto != undefined && data.credito_foto != '') {
          var creditoTopo = document.createElement('span');

          if (legendaTopo == undefined) {
            var legendaTopo = document.createElement('figcaption');
          }

          creditoTopo.appendChild(document.createTextNode(data.credito_foto));
          if (img.getAttribute('alt') != undefined) {
            img.setAttribute('alt', img.getAttribute('alt') + ' - ' + data.credito_foto);

            img.setAttribute('title', img.getAttribute('title') + ' - ' + data.credito_foto);
          } else {
            img.setAttribute('alt', data.credito_foto);

            img.setAttribute('title', data.credito_foto);
          }
        }
        if (legendaTopo != undefined) {
          // legendaTopo.appendChild(creditoTopo);

          foto.appendChild(legendaTopo);
        }
        gallery.appendChild(foto);
      });
      if (images.length > 1)
        gallery.innerHTML += '<button class="next"></button><button class="prev"></button>';

      document.querySelector('article').appendChild(gallery);

      var nextBtns = document.querySelectorAll('#gal' + (i + 1) + ' .next');

      for (idx = 0; idx < nextBtns.length; idx++) {
        nextBtns[idx].addEventListener('click', function() {
          if (document.querySelectorAll('#' + this.parentNode.id + ' .proximo').length > 0) {
            var atual = document.querySelector('#' + this.parentNode.id + ' .atual'),
              proximo = document.querySelector('#' + this.parentNode.id + ' .proximo');

            atual.classList.remove('atual');
            atual.classList.add('anterior');

            proximo.classList.remove('proximo');
            proximo.classList.add('atual');

            if (document.querySelectorAll('#' + this.parentNode.id + ' .proximo').length == 0) {
              this.style.display = 'none';
            }

            if (document.querySelectorAll('#' + this.parentNode.id + ' .anterior').length > 0) {
              document.querySelector('#' + this.parentNode.id + ' .prev').style.display = 'block';
            }
          }
        }, false);
      }

      var prevBtns = document.querySelectorAll('#gal' + (i + 1) + ' .prev');

      for (idx = 0; idx < prevBtns.length; idx++) {
        prevBtns[idx].addEventListener('click', function() {
          if (document.querySelectorAll('#' + this.parentNode.id + ' .anterior').length > 0) {
            var atual = document.querySelector('#' + this.parentNode.id + ' .atual'),
              anterior = document.querySelectorAll('#' + this.parentNode.id + ' .anterior')[document.querySelectorAll('#' + this.parentNode.id + ' .anterior').length - 1];

            atual.classList.remove('atual');
            atual.classList.add('proximo');

            anterior.classList.remove('anterior');
            anterior.classList.add('atual');

            if (document.querySelectorAll('#' + this.parentNode.id + ' .anterior').length == 0) {
              this.style.display = 'none';
            }

            if (document.querySelectorAll('#' + this.parentNode.id + ' .proximo').length > 0) {
              document.querySelector('#' + this.parentNode.id + ' .next').style.display = 'block';
            }
          }
        }, false);
      }

      artefolha.sobrinho.resize();

    } else if (data.imagem_topo != undefined && data.imagem_topo != '' && i > 0 && data.imagem_topo.indexOf('videos') > -1) {
      var div = document.createElement('div'),
        topVideo = document.createElement('video');

      if (artefolha.utils.isMobile()) {
        var mp4 = document.createElement('source'),
          webm = document.createElement('source');

        mp4.setAttribute('src', data.imagem_topo + 'low.mp4');
        webm.setAttribute('src', data.imagem_topo + 'sd.webm');

        topVideo.setAttribute('controls', 'controls');
      } else {
        var mp4 = document.createElement('source'),
          webm = document.createElement('source');

        mp4.setAttribute('src', data.imagem_topo + '.mp4');
        webm.setAttribute('src', data.imagem_topo + '.webm');

        topVideo.setAttribute('autoplay', 'autoplay');
      }

      topVideo.setAttribute('poster', data.imagem_topo.replace('videos', 'images') + '.jpg');
      topVideo.appendChild(mp4);
      topVideo.appendChild(webm);
      topVideo.setAttribute('muted', 'muted');
      topVideo.setAttribute('loop', 'loop');
      topVideo.setAttribute('width', '940');
      topVideo.setAttribute('height', '528');

      div.classList.add('gal');

      div.appendChild(topVideo);
      document.getElementsByTagName('article')[0].appendChild(div);
    }
  },
  expediente: function(texto) {
    var article = document.getElementsByTagName('article')[0],
      expediente = document.createElement('section'),
      hr = document.createElement('hr'),
      p = document.createElement('p');

    expediente.classList.add('texto');
    expediente.classList.add('expediente');
    texto.split('/').forEach(function(val, i) {
      var b = document.createElement('b');

      b.appendChild(document.createTextNode(val.slice(0, val.indexOf(':') + 1)));
      if (i > 0) {
        p.appendChild(document.createTextNode(' / '));
      }
      p.appendChild(b);
      p.appendChild(document.createTextNode(val.slice(val.indexOf(':') + 1, val.length)));
    });
    expediente.appendChild(hr);
    expediente.appendChild(p);

    article.appendChild(expediente);
  },
  esq: function(gal) {    if(gal.find('.anterior').length>0){
      gal.find('.atual').removeClass('atual').addClass('proximo');

      gal.find('.anterior').last().removeClass('anterior').addClass('atual');

      if(gal.find('.proximo').length>0){
        gal.find('.dir').fadeIn();
      }

      if(gal.find('.anterior').length==0){
        gal.find('.esq').fadeOut();
      }
    }
  },
  dir: function(gal) {
        if(gal.find('.proximo').length>0){
      gal.find('.atual').removeClass('atual').addClass('anterior');

      if(gal.find('.proximo').first().hasClass('lazy')) {
        gal.find('.proximo').first().find('img').attr('src', gal.find('.proximo').first().data('src'));
      }
      gal.find('.proximo').first().removeClass('proximo').addClass('atual');

      if(gal.find('.anterior').length>0){
        gal.find('.esq').fadeIn();
      }

      if(gal.find('.proximo').length==0){
        gal.find('.dir').fadeOut();
      }
    }
  },
  unlazy: function($this) {
    $this.parent().parent().removeClass('lazy');
  },
  dynamicClicks: function(esq, dir, gal) {
    esq.addEventListener('click', function() {
      artefolha.sobrinho.esq(this.parentNode);
    }, false);

    dir.addEventListener('click', function() {
      artefolha.sobrinho.dir(this.parentNode);
    }, false);

    if (artefolha.utils.isMobile()) {
      document.querySelector('#' + gal.id + ' .opener').addEventListener('click', function() {
        this.parentNode.classList.add('inuse');
        document.body.classList.add('no-scroll');
            $('.dir').css({"display":"none"})
      }, false);
/**/
      var overlay = artefolha.utils.getChildren(gal, 'overlay')[0],
        close = artefolha.utils.getChildren(overlay, 'close')[0];

      close.addEventListener('click', function() {
        this.parentNode.parentNode.classList.remove('inuse');
        document.getElementsByTagName('body')[0].classList.remove('no-scroll');
      }, false);

      overlay.addEventListener('touchstart', function(e) {
        touches.push(e.touches[0].clientX);
      });

      overlay.addEventListener('touchend', function(e) {
        touches.push(e.changedTouches[0].clientX);
        if ((touches[touches.length - 2] - touches[touches.length - 1]) > 100) {
          artefolha.sobrinho.dir(this);
        } else if ((touches[touches.length - 2] - touches[touches.length - 1]) < -100) {
          artefolha.sobrinho.esq(this);
        }
        touches = [];
      });
    }
    artefolha.sobrinho.resize();
  },
  mobileLoad: function() {
    var paragraphs = document.querySelectorAll('.delete'),
      paraUol = document.querySelectorAll('.delete-uol'),
      idx = paragraphs.length;

    for (key = 0; key < idx; key++) {
      if (paragraphs[key] != undefined) {
        if (paragraphs[key].textContent.indexOf('aspa') > -1) {
          artefolha.sobrinho.aspa(paragraphs[key]);
        } else if (paragraphs[key].textContent.indexOf('info') > -1) {
          artefolha.sobrinho.infos(paragraphs[key]);

        } else if (paragraphs[key].textContent.indexOf('galeria') > -1) {
          artefolha.sobrinho.galeriaMobile(paragraphs[key]);
        } else {
          paragraphs[key].parentNode.removeChild(paragraphs[key]);
        }
      }
    }

    $.each(paraUol, function(i, v) {
      artefolha.sobrinho.video(paraUol[i]);
    });
  }



};




$('#scroll').on( 'click',  function(){

$("html, body").animate({ scrollTop: $('#full1').height() }, "slow");
  $("body").removeClass('no-scroll')
  omtrHitCounter('fotos', "scroll");
  window.ivc('trackPageView');




});
$(document).on('click', '.esq', function() {
  artefolha.sobrinho.esq($(this).parent());
});

$(document).on('click', '.dir', function() {
  artefolha.sobrinho.dir($(this).parent());
});


$(document).on( 'mousemove','.antes-depois',  function(){



$('.antes').css({"width":event.clientX -  (($(window).width() - $(this).width())/2)+"px"})


});


$(document).on( 'touchmove','.antes-depois',  function(){



$('.antes').css({"width":event.changedTouches[0].pageX -  (($(window).width() - $(this).width())/2)+"px"})


});
$(document).on('click', '.fs-gallery', function(){
  if($(this).parent().parent().hasClass('overlay')){
    $(this).parent().parent().removeClass('overlay');
    if($(window).width()<940){
      $(this).parent().parent().find('.gal-img-container').height($(window).width() * 60 / 97);
    } else {
      $(this).parent().parent().find('.gal-img-container').height(940 * 60 / 97);
    }
    $(this).parent().parent().find('.gal-container').height($(this).parent().parent().find('.atual').innerHeight());
    $(this).parent().parent().find('.esq, .dir').height($(this).parent().parent().find('.atual .gal-img-container').innerHeight());
    $('body').removeClass('no-scroll');
    artefolha.sobrinho.resizeGallery();
  } else {
    $(this).parent().parent().addClass('overlay');
    $(this).parent().parent().find('.gal-container').removeAttr('style');
    $(this).parent().parent().find('.gal-img-container').removeAttr('style').css('height', '100%');
    $('body').addClass('no-scroll');
    $(this).parent().parent().find('.esq, .dir').css('height', '100%');
  }
});

$(document).on('click', '.overlay figure', function(e) {
  if(e.target.nodeName!='A') {
    $(this).parent().parent().toggleClass('clear-gallery');
  }
});

$(document).on( 'click','.menu',  function(){



$(this).addClass('opn')
$('.moda').addClass('opn')

});


  $(document).mousemove(function(event){

  })
$(document).on( 'click','.menu.opn',  function(){



$('.opn').removeClass('opn');

});



(function() {
    var interval = setInterval(function() {
        if (document.readyState == 'complete') {
          console.log(document.readyState)
          document.querySelector('.loading').style.display = 'none';
          
          document.querySelector('.loading').classList.add('fol-hide');
          artefolha.utils.scrollHash();
          clearInterval(interval);
        }
    },500)
})();