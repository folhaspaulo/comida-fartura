function isMobile(){
	if(navigator.userAgent.match(/Android/i)||navigator.userAgent.match(/webOS/i)||navigator.userAgent.match(/iPhone/i)||navigator.userAgent.match(/iPad/i)||navigator.userAgent.match(/iPod/i)||navigator.userAgent.match(/BlackBerry/i)||navigator.userAgent.match(/Windows Phone/i)){
		return true;
	}
	else {
		return false;
	}
}



(function(){
	var ww = document.getElementById('slide1').clientWidth;

	isMobile() ? document.getElementsByTagName('body')[0].classList.add('mobile') : document.getElementsByTagName('body')[0].classList.add('not-mobile');

	var interval=setInterval(function(){
		if(document.readyState=='complete'){
			document.getElementsByClassName('loading')[0].style.display = 'none';
			$('.antes').width($('.slider').css('left')).addClass('animate');
			$('.caption').each(function(){
				$(this).html($(this).parent().children('.img-container').children('.antes').data().caption);
			});
			$('.slider').addClass('animate').draggable({
				axis: 'x',
				containment:'parent',
				drag: function(e, ui){
					$(this).parent().children('.antes').width(ui.position.left+2);
					if(ui.position.left<ww/2){
						$(this).parent().parent().children('.caption').html($(this).parent().parent().children('.img-container').children('.depois').data().caption);
					} else {
						$(this).parent().parent().children('.caption').html($(this).parent().parent().children('.img-container').children('.antes').data().caption);
					}
				}
			});
			setTimeout(function(){
				$('#slide1 .slider').removeClass('animate').css('left',ww/2+'px');
				$('#slide1 .antes').removeClass('animate').css('width',ww/2+'px');
			}, 1000);
			setTimeout(function(){
				$('#slide2 .slider').removeClass('animate').css('left',ww/2+'px');
				$('#slide2 .antes').removeClass('animate').css('width',ww/2+'px');
			}, 1100);
			setTimeout(function(){
				$('#slide3 .slider').removeClass('animate').css('left',ww/2+'px');
				$('#slide3 .antes').removeClass('animate').css('width',ww/2+'px');
			}, 1200);
			setTimeout(function(){
				$('#slide4 .slider').removeClass('animate').css('left',ww/2+'px');
				$('#slide4 .antes').removeClass('animate').css('width',ww/2+'px');
			}, 1300);
			setTimeout(function(){
				$('#slide5 .slider').removeClass('animate').css('left',ww/2+'px');
				$('#slide5 .antes').removeClass('animate').css('width',ww/2+'px');
			}, 1400);
			clearInterval(interval);
		}
	}, 500);
})();