var 
	allGal = [],
	loadedGallery= [],
	touches = [];

var isMobile = function(){
	if(navigator.userAgent.match(/Android/i)||navigator.userAgent.match(/webOS/i)||navigator.userAgent.match(/iPhone/i)||navigator.userAgent.match(/iPad/i)||navigator.userAgent.match(/iPod/i)||navigator.userAgent.match(/BlackBerry/i)||navigator.userAgent.match(/Windows Phone/i)){
		return true;
	}
	else {
		return false;
	}
}

function cleanUp(value){
	return value.replace(/--/g, '\u2013')
				.replace(/\b'\b/g, "\u2019")
				.replace(/'\b/g, "\u2018")
				.replace(/'é/g,"\u2018é")
				.replace(/'É/g,"\u2018É")
				.replace(/'/g, "\u2019")
				.replace(/"\b/g, "\u201c")
				.replace(/"á/g,"\u201cá")
				.replace(/"é/g,"\u201cé")
				.replace(/"í/g,"\u201cí")
				.replace(/"ó/g,"\u201có")
				.replace(/"ú/g,"\u201cú")
				.replace(/"Á/g,"\u201cÁ")
				.replace(/"É/g,"\u201cÉ")
				.replace(/"Í/g,"\u201cÍ")
				.replace(/"Ó/g,"\u201cÓ")
				.replace(/"Ú/g,"\u201cÚ")
				.replace(/"\[/g,"\u201c[")
				.replace(/"/g, "\u201d")
				.replace(/\b",/g, "\u201d,")
				.replace(/m3/g,'m³')
				.replace(/m2/g,'m²')
				.replace(/O2/g,'O₂')
				.replace(/\u002E\u002E\u002E/g,'…')
				.replace(/&#8747;/g,'"');
}

function corrige(txt) {

	var result = ''

	if(txt.search('<')>=0 && txt.search('>')>=0){
		var text  = txt.split(/[<>]/);
		$.each(text, function(i, val){

			if(i==0){
				result+=cleanUp(val);
			} else if(i!=text.length-1){
				if(isEven(i)){
					result+='>'+cleanUp(val);
				} else {
					result+='<'+val;
				}

			} else {
				result+='>'+cleanUp(val);
			}

		});
	} else {
			result = cleanUp(txt);
	}

	return result;
}

function slug(nome) {
	var from = 'ãàáäâẽèéëêìíïîõòóöôùúüûñç·_,:;/-',
		to = 'aaaaaeeeeeiiiiooooouuuunc';

	nome = nome.toLowerCase().replace(/^\s+|\s+$/g, '').replace(/\s+/g, '').replace('.', '').replace(/'/g, '').replace(/\(/g, '').replace(/\)/g, '').replace(/\[/g, '').replace(/\]/g, '').replace(/\$/g, 's');
	for (var i = 0, l = from.length ; i < l ; i++) {
		nome = nome.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
	}
	return nome;
}


/* INICIO - Carrega Conteúdo*/
	function loadData(data){
		var content = document.getElementById("content");

			if (data.titulo != undefined && data.titulo != " ") {
				var titulo = document.createElement("H1");
				titulo.innerHTML=data.titulo;
				content.appendChild(titulo);

			}

			if (data.petiscos != undefined && data.petiscos.length > 0 ) {

				for (var i = 0; i < data.petiscos.length; i++) {
					var elem =  document.createElement("FIGURE"),
						elemTitle =  document.createElement("h2"),
						elemImg =  document.createElement("img");


						elemImg.setAttribute("src", data.petiscos[i].imagem)
						elem.appendChild(elemImg);




					for (var ii = 0; ii < data.petiscos[i].campos.length; ii++) {
						var textCont= document.createElement("figcaption"),
							text =  document.createElement("p"),
							textTitle =  document.createElement("span"),
							textTxt =  document.createTextNode(data.petiscos[i].campos[ii].texto),
							textTitleTxt =  document.createTextNode(data.petiscos[i].campos[ii].titulo+": ");



							textTitle.appendChild(textTitleTxt);
							text.appendChild(textTitle);
							text.appendChild(textTxt);

							textCont.appendChild(text);

							elem.appendChild(textCont);
					}

					content.appendChild(elem);
				}

			}

			
			


		}


(function(){
	$.ajax({
		url: url,
		dataType: 'jsonp',
		jsonpCallback: 'infoBack',
		contentType: 'text/plain',
		success: function(data) {
			loadData(data);

		}
	});
	var interval=setInterval(function(){
		if(document.readyState=='complete'){
			$('.loading').fadeOut(150);
			
			clearInterval(interval);
		}
	}, 500);
})();